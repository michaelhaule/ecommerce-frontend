export interface books {
id: number
title: string
author: string
isbn: string
pages: number
price: number
quantity: number
description: string
status: Boolean
dateCreated: Date

    // Id:number,
    // ProductName:string,
    // Brand:string,
    // Cost:number,
    // Type:string

}