import { Component } from '@angular/core';
import { Apollo, gql } from 'apollo-angular';
import { books } from './models/products';

const Get_Books = gql`
query{
  books{
    id
    title
    author
    isbn
    pages 
    price
    quantity
    description
    status
  }
}
`;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  allbooks:books[] = [];
  constructor(private apollo: Apollo){}
  ngOnInit(): void {
    this.apollo.watchQuery<any>({
      query: Get_Books
    })
    .valueChanges
    .subscribe(({data, loading}) => {
      alert("hello");
      console.log(loading);
      this.allbooks = data.books;
    })
  }
}
